﻿namespace com.FunJimChee.MonoComponentSystem.Example
{
    public sealed class ExampleMonoComponentManager : MonoComponentManagerBase<ExampleMonoComponentBase>
    {
        protected override void Init()
        {
            foreach (var component in Components)
            {
                //Call Component Setting() => reference ComponentManager
                component.Value.SettingManager(this);
            }
        }
    }
}