﻿namespace com.FunJimChee.MonoComponentSystem.Example
{
    using UnityEngine;

    public sealed class ValueControlComponent : ExampleMonoComponentBase
    {
        public float Number1;

        public float Number2;
    }

}