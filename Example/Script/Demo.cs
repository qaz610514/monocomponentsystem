﻿namespace com.FunJimChee.MonoComponentSystem.Example
{
    using System;
    using UnityEngine;

    public class Demo : MonoBehaviour
    {
        private ExampleMonoComponentManager Manager;

        private void Awake()
        {
            Manager = FindObjectOfType<ExampleMonoComponentManager>();
        }

        private void Start()
        {
            var valueComponent = Manager.GetMonoComponent<ValueControlComponent>();
            
            Debug.Log($"{valueComponent.Number1}/{valueComponent.Number2}");
        }
    }

}