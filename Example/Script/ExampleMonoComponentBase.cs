﻿namespace com.FunJimChee.MonoComponentSystem.Example
{
    public abstract class ExampleMonoComponentBase : MonoComponentBase
    {
        protected ExampleMonoComponentManager Manager;

        public void SettingManager(ExampleMonoComponentManager manager)
        {
            Manager = manager;
        }
    }
}