﻿namespace com.FunJimChee.MonoComponentSystem.Example
{
    using UnityEngine.UI;
    using System.Globalization;

    public sealed class TextUIComponent : ExampleMonoComponentBase
    {
        private bool _isSafe;
        
        private ValueControlComponent _valueComponent;
        
        public Text Number1Text;

        public Text Number2Text;

        protected override void OnAwake()
        {
            _valueComponent = Manager.GetMonoComponent<ValueControlComponent>();

            if (_valueComponent != null)
            {
                _isSafe = true;
            }

            if (!_isSafe)
            {
                return;
            }

            Number1Text.text = _valueComponent.Number1.ToString(CultureInfo.InvariantCulture);

            Number2Text.text = _valueComponent.Number2.ToString(CultureInfo.InvariantCulture);
        }
    }

}