﻿namespace com.FunJimChee.MonoComponentSystem.Example
{
    using UnityEngine.UI;
    using System.Globalization;

    public sealed class ButtonUIComponent : ExampleMonoComponentBase
    {
        private bool _isSafe;

        private TextUIComponent _textUIComponent;

        private ValueControlComponent _valueComponent;

        public Button Number1Btn;

        public Button Number2Btn;

        protected override void OnAwake()
        {
            _textUIComponent = Manager.GetMonoComponent<TextUIComponent>();

            _valueComponent = Manager.GetMonoComponent<ValueControlComponent>();

            if (_textUIComponent != null && _valueComponent != null) _isSafe = true;

            if (!_isSafe)
            {
                return;
            }

            Number1Btn.onClick.AddListener(() =>
            {
                if (!_isSafe)
                {
                    return;
                }

                _valueComponent.Number1++;
                _textUIComponent.Number1Text.text = _valueComponent.Number1.ToString(CultureInfo.InvariantCulture);
            });

            Number2Btn.onClick.AddListener(() =>
            {
                if (!_isSafe)
                {
                    return;
                }

                _valueComponent.Number2++;
                _textUIComponent.Number2Text.text = _valueComponent.Number2.ToString(CultureInfo.InvariantCulture);
            });
        }
    }
}