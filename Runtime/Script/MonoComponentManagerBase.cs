﻿namespace com.FunJimChee.MonoComponentSystem
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using System.Reflection;

    public abstract class MonoComponentManagerBase<T> : MonoBehaviour where T : MonoComponentBase
    {
        protected Dictionary<Type, T> Components;

        private void Awake()
        {
            StartWork();
        }

        private void StartWork()
        {
            //Init Dictionary
            Components = new Dictionary<Type, T>();

            var types = GetType().Assembly.GetTypes().Where(t => t.IsSubclassOf(typeof(T)) && !t.IsAbstract);

            foreach (var type in types)
            {
                var instances = FindObjectsOfType(type);

                if (instances.Length == 0)
                {
                    Components.Add(type, gameObject.AddComponent(type) as T);
                }

                if (instances.Length > 0)
                {
                    Components.Add(type, instances.FirstOrDefault() as T);
                }

                if (instances.Length > 1)
                {
                    Debug.LogWarning($"{type.Name} has more than one! It's only setting for default component.");
                }
            }

            Init();

            foreach (var component in Components)
            {
                //Call Component OnAwake()
                
                var method = component.Key.GetMethod("OnAwake", BindingFlags.NonPublic | BindingFlags.Instance);
                
                method?.Invoke(component.Value, new object[0]);
            }

            foreach (var component in Components)
            {
                //Call Component OnStart()
                
                var method = component.Key.GetMethod("OnStart", BindingFlags.NonPublic | BindingFlags.Instance);
                
                method?.Invoke(component.Value, new object[0]);
            }
        }

        protected virtual void Init()
        {
        }

        public TC GetMonoComponent<TC>() where TC : T
        {
            if (Components.ContainsKey(typeof(TC)))
            {
                return Components[typeof(TC)] as TC;
            }

            return default;
        }
    }
}