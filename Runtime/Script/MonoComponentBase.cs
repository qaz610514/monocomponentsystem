﻿namespace com.FunJimChee.MonoComponentSystem
{
    using UnityEngine;
    public abstract class MonoComponentBase : MonoBehaviour
    {
        protected virtual void OnAwake()
        {
            
        }

        protected virtual void OnStart()
        {
            
        }
    }
}